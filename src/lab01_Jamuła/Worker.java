package lab01_Jamu�a;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.UUID;
//Bruno Jamu�a 210370

public class Worker extends UnicastRemoteObject implements IWorker{
	class Task
	{
	    public 	Duration duration; 
	    public String name; 
	    public Object o;
	    Task(Duration d, String n, Object a){
	    	duration = d;
	    	name = n;
	    	o=a;
	    }
	 };
	ArrayList<Task> tasks = new ArrayList<Task>();
	int duration;
	float completeTime;
	Duration d = Duration.ofMillis(0);
	Duration helper;
	String task;
	IClient klient;
	protected Worker() throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
	}
	

	@Override
	public boolean setTask(String task, Duration d, Object o) throws RemoteException {
		if(tasks.size()==0 && this.d.toMillis()==0){
			helper =Duration.ofMillis(0);
		klient = (IClient) o;
		this.d = d;
		completeTime=d.toMillis();
		this.task = task;
		new Thread(new Runnable(){

			@Override
			public void run() {
				 Duration duration;
			        System.out.println("wykonuje zadanie: "+task);
			        Instant first = Instant.now();
					 do{
				            try {
				                Thread.sleep(1);
				            } catch (InterruptedException e) {
				                e.printStackTrace();
				            }
				            Instant second = Instant.now();
				            duration = Duration.between(first, second);
				            update(duration);
				            helper = duration;
				        }while(d.toMillis()>duration.toMillis());
				        System.out.println("wykonalem: "+task);
				        try {
							klient.setResult(task);
						} catch (RemoteException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
				        if(!tasks.isEmpty()){
				        	
								try {
									check(tasks.get(0).name,tasks.get(0).duration,tasks.get(0).o);
								} catch (RemoteException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							
							
				        
				
			}
						zero();

			
		}
		}).start();
		}else{
			System.out.println("jestem");
			completeTime=completeTime + d.toMillis();
			tasks.add(new Task(d,task,o));
			System.out.println(tasks.size());
		}
		return false;
	}
	public void zero(){
		this.d = Duration.ofMillis(0);
	}
	public void update(Duration duration){
		this.completeTime = this.completeTime - duration.toMillis()+helper.toMillis();
		this.d = duration;
	}
	public void check(String task, Duration d, Object o) throws RemoteException{
		tasks.remove(0);
		this.d = Duration.ofMillis(0);
		completeTime=completeTime - this.d.toMillis();
		this.setTask(task, d, o);
	}
	@Override
	public String getState() throws RemoteException {
		if(this.d.toMillis()==0){
			return "wolny";
		}
		else{
			System.out.println("worer zajety " + completeTime);
			return "zajety"+" "+ completeTime;
		}
	}

	@Override
	public int getNumber() throws RemoteException {
		// TODO Auto-generated method stub
		return duration;
	}

	public static void main(String[] args) throws RemoteException {
		int port = Integer.parseInt(args[0]) ;
		String localhost = (args[2]);
		IWorker pracownik = new Worker();
		  try { 
		        Registry registry =  LocateRegistry.getRegistry(localhost,port);
		        IRegistry reg = (IRegistry) registry.lookup(args[1]);
		        reg.registerObject(pracownik);
		        System.out.println("dzialam");
		        
			  } catch (Exception e) { 
			         System.err.println("MyInterfaceImpl excep."+ e.getMessage()); 
			  } 
		  

	}


//	@Override
//	public void run() {
//		 Duration duration;
//        System.out.println("wykonuje zadanie: "+task);
//        Instant first = Instant.now();
//		 do{
//	            try {
//	                Thread.sleep(1);
//	            } catch (InterruptedException e) {
//	                e.printStackTrace();
//	            }
//	            Instant second = Instant.now();
//	            
//	            duration = Duration.between(first, second);
//	            System.out.println(duration.toString());
//	            System.out.println(d.toString());
//	        }while(d.toMillis()>duration.toMillis());
//	        System.out.println("wykonalem: "+task);
//	        try {
//				klient.setResult(task);
//			} catch (RemoteException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//	        if(!tasks.isEmpty()){
//	        	try {
//					this.setTask(tasks.get(0).name, tasks.get(0).duration, tasks.get(0).o);
//					tasks.remove(0);
//				} catch (RemoteException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//	        }
//		
//	}

}
