package lab01_Jamu�a;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.time.Duration;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
//Bruno Jamu�a 210370

public class Client extends UnicastRemoteObject implements IClient {
	int duration;
	String state;
    static JTextArea completed = new JTextArea();

	
	protected Client() throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
	}
	





	public static void main(String[] args) throws RemoteException {
		int port = Integer.parseInt(args[0]) ;
		String localhost = (args[2]);
		IClient pracownik = new Client();
        Duration duration = Duration.ofMillis(10000);
        long a = duration.toMillis();
        System.out.println(a);
		
		  try { 
		        Registry registry =  LocateRegistry.getRegistry(localhost,port);
		        IRegistry reg = (IRegistry) registry.lookup(args[1]);
		        System.out.println("dzialam");
		        IManager menago = (IManager) reg.getManager();

		        JFrame frame = new JFrame("Absolute Layout Example");
		        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		        JPanel contentPane = new JPanel();
		        contentPane.setOpaque(true);
		        contentPane.setLayout(null);

		        JLabel label = new JLabel(
		            "Podaj zadanie"
		                                    , JLabel.CENTER);
		        label.setSize(100, 30);
		        label.setLocation(5, 5);

		        JButton addTask = new JButton("dodaj");
		        addTask.setSize(100, 30);
		        addTask.setLocation(95, 45);
		        
		        JTextArea task = new JTextArea(2,10);
		        task.setSize(100,20 );
		        task.setLocation(105, 10);
		        
		        completed.setSize(100,400);
		        completed.setLocation(220, 10);
		        
		        contentPane.add(label);
		        contentPane.add(addTask);
		        contentPane.add(task);
		        contentPane.add(completed);
		        frame.setContentPane(contentPane);
		        frame.setSize(310, 425);
		        frame.setLocationByPlatform(true);
		        frame.setVisible(true);
		        addTask.addActionListener(ae -> {
		        	try {
		        		menago.assignTask(task.getText(), duration, pracownik);
		        		task.setText(null);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} 
		        	
			  });
		        } catch (Exception e) { 
			         System.err.println("MyInterfaceImpl excep."+ e.getMessage()); 
			  } 
		  

	}


	@Override
	public void setResult(String r) {
		completed.append(r+"\n");
	}

}
