package lab01_Jamu�a;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.time.Duration;
import java.util.ArrayList;
//Bruno Jamu�a 210370

//lookup rmiregistry nazwa, host, port [ to jeszcze musi wiedziec gdize - localhost]
interface IWorker extends Remote{
  boolean setTask(String task, Duration d, Object o) throws RemoteException; // o to namiastka na klienta
  String getState() throws RemoteException; //"Liczba zadan, sumaryczny czas (MS), referencja na klienta"
  int getNumber() throws RemoteException;  //"kazdy workerk ma identyfikator"
}

//rebind rmiregistry (nazwa i port)-atrybuty  [to sie twrozy na lokalnej maszynie]
interface IRegistry extends Remote{
  int registerObject(Object o) throws RemoteException;  /// rejestruje obiekty
  boolean unregisterObject(int number) throws RemoteException;
  Object getManager() throws RemoteException;
  ArrayList<Object> getWorkers() throws RemoteException; //IWorker.class.isInstance(o)
}


//lookup rmiregistry nazwa, host, port
interface IManager extends Remote {
  void assignTask(String task, Duration d, Object o) throws RemoteException; //przeglada liste znajduje worker min czasem do wyklonania i przydziela
  void refresh() throws RemoteException;   // refreshuje okno z zadaniami ktore sie wykonaly
}


//klient ma gui ktore wysyla taski 
interface IClient extends Remote {
  void setResult(String r) throws RemoteException;
}