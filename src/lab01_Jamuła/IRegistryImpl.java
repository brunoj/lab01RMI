package lab01_Jamu�a;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.rmi.*; 
import java.rmi.server.*; 
//Bruno Jamu�a 210370

public class IRegistryImpl extends UnicastRemoteObject implements IRegistry {
	static ArrayList<Object> workers;
	 IManager menago;
	 IWorker worker;
	protected IRegistryImpl() throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public int registerObject(Object o) throws RemoteException {
		if(o instanceof IWorker){
			workers.add(o);
			System.out.println("Dodalem pracownika nr " + workers.size());
			 worker = (IWorker) workers.get(0);
			
		}else{
			menago = (IManager) o;
			System.out.println("dodalem menadzera");
		}
		return 0;
	}

	@Override
	public boolean unregisterObject(int number) throws RemoteException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Object getManager() throws RemoteException {
		// TODO Auto-generated method stub
		return menago;
	}

	@Override
	public ArrayList<Object> getWorkers() throws RemoteException {
		
		return workers;
	}

	public static void main(String[] args) {

		int port = Integer.parseInt(args[0]) ;
        try {
        	IRegistryImpl  register = new  IRegistryImpl ();
            Registry registry =  LocateRegistry.createRegistry(port);
            registry.rebind(args[1], register);
            workers = new ArrayList<Object>();

            System.out.println("Sukces");

       } catch (Exception e) {
            e.printStackTrace();
       }

	}

}
